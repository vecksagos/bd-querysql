package test;

import java.util.List;

import junit.framework.TestCase;
import model.Participar;

import org.hibernate.Session;

import persistence.dao.hibernate.TenistaHDao;
import persistence.dao.hibernate.TorneioHDao;
import persistence.dao.jdbc.TenistaDao;
import persistence.dao.jdbc.TorneioDao;
import util.HConexao;

public class TestDao extends TestCase {
	
	public void testTenistaSelectAll() {
		TenistaHDao hdao = new TenistaHDao();
		TenistaDao jdao = new TenistaDao();
		assertEquals(hdao.select().size(), jdao.select().size());
	}
	
	public void testTorneioSelectAll() {
		TorneioHDao hdao = new TorneioHDao();
		TorneioDao jdao = new TorneioDao();
		assertEquals(hdao.select().size(), jdao.select().size());
	}

	
}
