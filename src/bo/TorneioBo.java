package bo;

import java.util.List;

import model.Participar;
import model.Torneio;
import persistence.TorneioPersistence;
import persistence.dao.hibernate.TorneioHDao;
import persistence.dao.jdbc.ParticiparDao;

public class TorneioBo {
	private  static TorneioPersistence dao;

	public TorneioBo() {
		if (dao == null) 
			dao = new TorneioHDao();
	}
	
	public void insert(Torneio torneio) {
		dao.insert(torneio);
	}

	public List<Torneio> selectAll() {
		return dao.select();
	}

	public void update(Torneio torneio) {
		dao.update(torneio);
	}

	public void delete(Torneio torneio) {
		dao.delete(torneio);
	}

	public Torneio getById(int id) {
		return dao.getTorneio(id);
	}
	
	public List<Participar> deletaTorneio(Torneio torneio) {
		ParticiparDao participar = new ParticiparDao();
		return participar.deletaTorneio(torneio);
	}
}
