package bo;

import java.util.List;

import persistence.dao.jdbc.ParticiparDao;
import model.Participar;
import model.Tenista;
import model.Torneio;

public class ParticiparBo {
	private static ParticiparDao dao;

	public ParticiparBo() {
		if (dao == null) 
			dao = new ParticiparDao();
	}

	public int insert(Participar participar) {
		return dao.insert(participar);
	}

	public List<Participar> selectAll() {
		return dao.select();
	}

	public int update(Participar participar) {
		return dao.update(participar);
	}

	public int delete(Participar participar) {
		return dao.delete(participar);
	}

	public Participar getOne(Tenista tenista, Torneio torneio, int ano) {
		return dao.getParticipar(tenista, torneio, ano);
	}
	
	public List<Participar> get2009() {
		return dao.deletar2009();
	}
}
