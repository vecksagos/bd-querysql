package bo;

import java.util.List;

import model.Participar;
import model.Tenista;
import persistence.TenistaPersistence;
import persistence.dao.hibernate.TenistaHDao;
import persistence.dao.jdbc.ParticiparDao;

public class TenistaBo {
	
	private static TenistaPersistence persistence;
	
	public TenistaBo() {
		if (persistence == null) 
			persistence = new TenistaHDao();
	}
	
	public void insert(Tenista tenista) {
		persistence.insert(tenista);
	}
	
	public List<Tenista> selectAll() {
		return persistence.select();
	}
	
	public void update(Tenista tenista) {
		persistence.update(tenista);
	}
	
	public void delete(Tenista tenista) {
		List<Tenista> lista = persistence.getApadrinhado(tenista);
		for (Tenista te : lista)
			update(te);
		persistence.delete(tenista);
	}
	
	public Tenista getById(int id) {
		return persistence.getTenista(id);
	}
	
	public List<Participar> deletaTenista(Tenista tenista) {
		ParticiparDao participar = new ParticiparDao();
		return participar.deletaTenista(tenista);
	}
}
