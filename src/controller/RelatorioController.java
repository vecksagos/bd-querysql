package controller;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import util.Conexao;

public class RelatorioController {
	public void criarRelatorio1(boolean exportar) {

		// compilacao do JRXML
		try {
			JasperReport report = JasperCompileManager
					.compileReport("relatorios/relatorioTenista.jrxml");

			//JRBeanCollectionDataSource
			JasperPrint print = JasperFillManager.fillReport(report, null,
					Conexao.getConnection());

			//exportacao do relatorio para outro formato, no caso PDF
			if (exportar)
			JasperExportManager.exportReportToPdfFile(print,
					"relatorios/relatorioTenista.pdf");
			
			JasperViewer.viewReport(print, false);

		} catch (JRException e) {
			e.printStackTrace();
		}
	}
	
	public void criarRelatorio2(boolean exportar) {

		// compilacao do JRXML
		try {
			JasperReport report = JasperCompileManager
					.compileReport("relatorios/tenistaPorTorneio.jrxml");

			//JRBeanCollectionDataSource
			JasperPrint print = JasperFillManager.fillReport(report, null,
					Conexao.getConnection());

			//exportacao do relatorio para outro formato, no caso PDF
			if (exportar)
			JasperExportManager.exportReportToPdfFile(print,
					"relatorios/tenistaPorTorneio.pdf");
			
			JasperViewer.viewReport(print, false);

		} catch (JRException e) {
			e.printStackTrace();
		}
	}
	
	public void criarRelatorio3(boolean exportar) {

		// compilacao do JRXML
		try {
			JasperReport report = JasperCompileManager
					.compileReport("relatorios/torneioDisputados.jrxml");

			//JRBeanCollectionDataSource
			JasperPrint print = JasperFillManager.fillReport(report, null,
					Conexao.getConnection());

			//exportacao do relatorio para outro formato, no caso PDF
			if (exportar)
			JasperExportManager.exportReportToPdfFile(print,
					"relatorios/torneioDisputados.pdf");
			
			JasperViewer.viewReport(print, false);

		} catch (JRException e) {
			e.printStackTrace();
		}
	}
}
