package controller;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import model.Participar;
import model.Torneio;
import bo.ParticiparBo;
import bo.TorneioBo;

public class TorneioController {

	public Object[][] tabelar() {
		TorneioBo bo = new TorneioBo();
		List<Torneio> lista = bo.selectAll();

		Object[][] obj = new Object[lista.size()][10];
		int i = 0;
		for (Torneio torneio : lista) {
			obj[i][0] = torneio.getId();
			obj[i][1] = torneio.getNome();
			obj[i][2] = torneio.getLocal();
			obj[i][3] = false;
			++i;
		}

		return obj;
	}

	public void salvarTabela(JTable table) {
		List<Torneio> lista = new ArrayList<Torneio>();
		TorneioBo bo = new TorneioBo();

		for (int i = 0; i < table.getRowCount(); ++i) {
			Torneio torneio = new Torneio();
			torneio.setId((int) table.getValueAt(i, 0));
			torneio.setNome((String) table.getValueAt(i, 1));
			torneio.setLocal((String) table.getValueAt(i, 2));
			lista.add(torneio);
		}

		for (Torneio torneio : lista) 
			bo.update(torneio);
	}

	public void deletarTorneios(JTable table) {
		List<Torneio> lista = new ArrayList<Torneio>();
		TorneioBo bo = new TorneioBo();


		List<List<Participar>> participar = new ArrayList<List<Participar>>();
		
		for (int i = 0; i < table.getRowCount(); ++i) {
			Torneio torneio = new Torneio();
			if ((boolean) table.getValueAt(i, 3)) {
				torneio.setId((int) table.getValueAt(i, 0));
				lista.add(torneio);
				
				participar.add(bo.deletaTorneio(torneio));
			}
		}

		for (Torneio torneio : lista)
			bo.delete(torneio);
		
		ParticiparBo participarBo = new ParticiparBo();
		for (List<Participar> p : participar)
			for (Participar par : p)
				participarBo.delete(par);
	}

	public void inserirTorneio(Torneio torneio) {
		TorneioBo bo = new TorneioBo();
		bo.insert(torneio);
	}
}
