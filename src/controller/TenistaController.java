package controller;


import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import model.Participar;
import model.Tenista;
import bo.ParticiparBo;
import bo.TenistaBo;

public class TenistaController {

	public Object[][] tabelar() {
		TenistaBo tenistaBo = new TenistaBo();
		List<Tenista> lista = tenistaBo.selectAll();

		Object[][] obj = new Object[lista.size()][10];
		int i = 0;
		for (Tenista tenista : lista) {
			obj[i][0] = tenista.getId();
			obj[i][1] = tenista.getNome();
			obj[i][2] = tenista.getApelido();
			obj[i][3] = tenista.getAno_nasc();
			obj[i][4] = tenista.getCidade_nascimento();
			obj[i][5] = tenista.getCidade_moradia();
			obj[i][6] = (tenista.isEsta_ativo()) ? "Sim" : "Não";
			obj[i][7] = (tenista.getPadrinho() != null) ? tenista.getPadrinho().getId() : 0;
			obj[i][8] = (tenista.getPadrinho() != null) ? tenista.getPadrinho().getNome() : "Ninguem";
			obj[i][9] = false;
			++i;
		}

		return obj;
	}

	public void salvarTabela(JTable table) {
		List<Tenista> lista = new ArrayList<Tenista>();
		TenistaBo bo = new TenistaBo();

		for (int i = 0; i < table.getRowCount(); ++i) {
			Tenista tenista = new Tenista();
			tenista.setId((int) table.getValueAt(i, 0));
			tenista.setNome((String) table.getValueAt(i, 1));
			tenista.setApelido((String) table.getValueAt(i, 2));
			tenista.setAno_nasc((int) table.getValueAt(i, 3));
			tenista.setCidade_nascimento((String) table.getValueAt(i, 4));
			tenista.setCidade_moradia((String) table.getValueAt(i, 5));
			tenista.setEsta_ativo((((String) table.getValueAt(i, 6)).matches("Sim")) ? true : false);
			tenista.setPadrinho(bo.getById((int) table.getValueAt(i, 7)));
			lista.add(tenista);
		}

		for (Tenista tenista : lista) 
			bo.update(tenista);
	}

	public void deletarTenistas(JTable table) {
		List<Tenista> lista = new ArrayList<Tenista>();
		TenistaBo bo = new TenistaBo();

		List<List<Participar>> participar = new ArrayList<List<Participar>>();

		for (int i = 0; i < table.getRowCount(); ++i) {
			Tenista tenista = new Tenista();
			if ((boolean) table.getValueAt(i, 9)) {
				tenista.setId((int) table.getValueAt(i, 0));
				lista.add(tenista);
				
				participar.add(bo.deletaTenista(tenista));
			}
		}

		for (Tenista tenista : lista)
			bo.delete(tenista);
		
		ParticiparBo participarBo = new ParticiparBo();
		for (List<Participar> p : participar)
			for (Participar par : p)
				participarBo.delete(par);
	}

	public void inserirTenista(Tenista tenista) {
		TenistaBo bo = new TenistaBo();
		bo.insert(tenista);
	}
	
	public void criar5() {
		List<Tenista> lista = new ArrayList<Tenista>();
		TenistaBo bo = new TenistaBo();
		
		Tenista t1 = new Tenista();
		t1.setAno_nasc(1999);
		t1.setApelido("Professor XXX");
		t1.setCidade_moradia("Escola para mutantes especiais");
		t1.setCidade_nascimento("Em algum lugar");
		t1.setEsta_ativo(true);
		t1.setNome("Xavier");
		
		Tenista t2 = new Tenista();
		t2.setAno_nasc(1457);
		t2.setApelido("NEO The chosen one");
		t2.setCidade_moradia("America fuck yeah");
		t2.setCidade_nascimento("Matrix");
		t2.setEsta_ativo(false);
		t2.setNome("Mister Anderson");
		
		Tenista t3 = new Tenista();
		t3.setAno_nasc(2030);
		t3.setApelido("Try");
		t3.setCidade_moradia("Restaurante do fim do universo");
		t3.setCidade_nascimento("Nova Inglaterra");
		t3.setEsta_ativo(true);
		t3.setNome("Trillian");
		t3.setPadrinho(bo.getById(3));
		
		Tenista t4 = new Tenista();
		t4.setAno_nasc(2030);
		t4.setApelido("O Viajante");
		t4.setCidade_moradia("Nova Maceió");
		t4.setCidade_nascimento("Nova Arapiraca");
		t4.setEsta_ativo(true);
		t4.setNome("Arthur Dent");
		
		Tenista t5 = new Tenista();
		t5.setAno_nasc(0);
		t5.setApelido("Chezus");
		t5.setCidade_moradia("Nazaré");
		t5.setCidade_nascimento("Nazaré");
		t5.setEsta_ativo(true);
		t5.setNome("Jesus");
		t5.setPadrinho(bo.getById(1));
		
		lista.add(t1);
		lista.add(t2);
		lista.add(t3);
		lista.add(t4);
		lista.add(t5);
		
		for (Tenista tenista : lista) 
			bo.insert(tenista);
	}
}
