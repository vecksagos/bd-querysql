package controller;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import model.Participar;
import model.Tenista;
import model.Torneio;
import bo.ParticiparBo;
import bo.TenistaBo;
import bo.TorneioBo;

public class ParticiparController {

	public Object[][] tabelar() {
		ParticiparBo bo = new ParticiparBo();
		List<Participar> lista = bo.selectAll();

		Object[][] obj = new Object[lista.size()][10];
		int i = 0;
		for (Participar participar : lista) {
			obj[i][0] = participar.getTenista().getId();
			obj[i][1] = participar.getTenista().getNome();
			obj[i][2] = participar.getTorneio().getId();
			obj[i][3] = participar.getTorneio().getNome();
			obj[i][4] = participar.getAno();
			obj[i][5] = participar.getPremio();
			obj[i][6] = (participar.isFoi_finalista()) ? "Sim" : "Não";
			obj[i][7] = (participar.isFoi_campeao()) ? "Sim" : "Não";
			obj[i][8] = false;
			++i;
		}

		return obj;
	}

	public void salvarParticipar(JTable table) {
		List<Participar> lista = new ArrayList<Participar>();
		ParticiparBo bo = new ParticiparBo();
		TenistaBo tenistaBo = new TenistaBo();
		TorneioBo torneioBo = new TorneioBo();

		for (int i = 0; i < table.getRowCount(); ++i) {
			Participar participar = new Participar();
			participar.setTenista(tenistaBo.getById((int) table.getValueAt(i, 0)));
			participar.setTorneio(torneioBo.getById((int) table.getValueAt(i, 2)));
			participar.setAno((int) table.getValueAt(i, 4));
			participar.setPremio((double) table.getValueAt(i, 5));
			participar.setFoi_finalista(((String) table.getValueAt(i, 6)).matches("Sim") ? true : false);
			participar.setFoi_campeao(((String) table.getValueAt(i, 7)).matches("Sim") ? true : false);
			lista.add(participar);
		}

		for (Participar participar : lista) 
			bo.update(participar);
	}

	public void deletarParticipar(JTable table) {
		List<Participar> lista = new ArrayList<Participar>();
		ParticiparBo bo = new ParticiparBo();
		TenistaBo tenistaBo = new TenistaBo();
		TorneioBo torneioBo = new TorneioBo();

		for (int i = 0; i < table.getRowCount(); ++i) {
			Participar participar = new Participar();
			if ((boolean) table.getValueAt(i, 8)) {
				participar.setTenista(tenistaBo.getById((int) table.getValueAt(i, 0)));
				participar.setTorneio(torneioBo.getById((int) table.getValueAt(i, 2)));
				participar.setAno((int) table.getValueAt(i, 4));
				lista.add(participar);
			}
		}

		for (Participar participar : lista)
			bo.delete(participar);
	}

	public void inserirParticipar(Participar participar) {
		ParticiparBo bo = new ParticiparBo();
		bo.insert(participar);
	}

	public Integer[] tenistaBox() {
		TenistaBo tenistaBo = new TenistaBo();
		List<Tenista> lista = tenistaBo.selectAll();

		Integer[] ids = new Integer[lista.size()];

		int i = 0;
		for (Tenista tenista : lista) { 
			ids[i] = tenista.getId();
			++i;
		}
		
		return ids;
	}
	
	public Integer[] torneioBox() {
		TorneioBo torneioBo = new TorneioBo();
		List<Torneio> lista = torneioBo.selectAll();

		Integer[] ids = new Integer[lista.size()];

		int i = 0;
		for (Torneio torneio : lista) { 
			ids[i] = torneio.getId();
			++i;
		}
		
		return ids;
	}
	
	public void delete2009() {
		ParticiparBo bo = new ParticiparBo();
		List<Participar> lista = bo.get2009();
		
		for (Participar pa : lista) 
			bo.delete(pa);
	}
}
