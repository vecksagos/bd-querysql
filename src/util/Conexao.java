package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

	private static Connection con;

	public static Connection getConnection() {

		if (con == null)
			try {
				Class.forName("com.mysql.jdbc.Driver");    
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tenis", "root", "senhaki");
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

		return con;
	}

	public static void close() {
		if (con != null) 
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

	}
}
