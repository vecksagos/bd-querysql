package persistence;

import java.util.List;

import model.Torneio;

public interface TorneioPersistence {

	void insert(Torneio torneio);
	List<Torneio> select();
	void update(Torneio Torneio);
	void delete(Torneio Torneio);
	Torneio getTorneio(int id);
}
