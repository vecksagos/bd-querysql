package persistence.dao.hibernate;

import java.util.List;

import model.Tenista;

import org.hibernate.Session;

import persistence.TenistaPersistence;
import util.HConexao;

public class TenistaHDao implements TenistaPersistence {

	@Override
	public void insert(Tenista tenista) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(tenista);
		session.getTransaction().commit();
		session.close();
	}
	
	@Override
	public List<Tenista> select() {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		
		List<Tenista> lista = (List<Tenista>) session.createQuery("FROM Tenista").list();
		
		session.getTransaction().commit();
		session.close();
		
		return lista;
	}
	
	@Override
	public void update(Tenista tenista) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		session.update(tenista);
		session.getTransaction().commit();
		session.close();
	}

	@Override
	public void delete(Tenista tenista) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		session.delete(tenista);
		session.getTransaction().commit();
		session.close();
	}	

	@Override
	public Tenista getTenista(int id) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		
		List<Tenista> lista = (List<Tenista>) session.createQuery("FROM Tenista WHERE id=:idTenista").setParameter("idTenista", id).list();
		Tenista tenista = (Tenista) ((lista.size() != 0) ? lista.get(0) : null);
		
		session.getTransaction().commit();
		session.close();
		
		return tenista;
	}

	@Override
	public List<Tenista> getApadrinhado(Tenista tenista) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		
		List<Tenista> lista = (List<Tenista>) session.createQuery("FROM Tenista").list();
		
		session.getTransaction().commit();
		session.close();
		
		return lista;
	}
}
