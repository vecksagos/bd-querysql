package persistence.dao.hibernate;

import java.util.List;

import org.hibernate.Session;

import model.Participar;
import model.Tenista;
import model.Torneio;
import persistence.ParticiparPersistence;
import util.HConexao;

public class ParticiparHDao implements ParticiparPersistence {

	@Override
	public void insert(Participar participar) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(participar);
		session.getTransaction().commit();
		session.close();
	}

	@Override
	public List<Participar> select() {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();

		List<Participar> lista = (List<Participar>) session.createQuery("FROM Participar").list(); 

		session.getTransaction().commit();
		session.close();

		return lista;
	}

	@Override
	public void update(Participar participar) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		session.update(participar);
		session.getTransaction().commit();
		session.close();
	}

	@Override
	public void delete(Participar participar) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		session.delete(participar);
		session.getTransaction().commit();
		session.close();
	}

	@Override
	public Participar getParticipar(Tenista tenista, Torneio torneio, int ano) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		List<Participar> lista = (List<Participar>) session.createQuery("FROM Participar WHERE "
				+ "id_tenista=:idTenista AND "
				+ "id_torneio=:idTorneio "
				+ "AND ano=:date")
				.setParameter("idTenista", tenista.getId())
				.setParameter("idTorneio", torneio.getId())
				.setParameter("date", ano).list();
		
		Participar participar = (Participar) ((lista.size() != 0) ? lista.get(0) : null); 

		session.getTransaction().commit();
		session.close();

		return participar;
	}

	@Override
	public List<Participar> deletaTorneio(Torneio torneio) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Participar> deletaTenista(Tenista tenista) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Participar> deleta2009() {
		// TODO Auto-generated method stub
		return null;
	}

}
