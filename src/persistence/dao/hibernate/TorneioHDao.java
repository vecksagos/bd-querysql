package persistence.dao.hibernate;

import java.util.List;

import model.Torneio;

import org.hibernate.Session;

import persistence.TorneioPersistence;
import util.HConexao;

public class TorneioHDao implements TorneioPersistence {
	
	@Override
	public void insert(Torneio torneio) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(torneio);
		session.getTransaction().commit();
		session.close();
	}

	@Override
	public List<Torneio> select() {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		
		List<Torneio> lista = (List<Torneio>) session.createQuery("FROM Torneio").list();
		
		session.getTransaction().commit();
		session.close();
		
		return lista;
	}

	@Override
	public void update(Torneio torneio) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		session.update(torneio);
		session.getTransaction().commit();
		session.close();
	}

	@Override
	public void delete(Torneio torneio) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		session.delete(torneio);
		session.getTransaction().commit();
		session.close();
	}	

	@Override
	public Torneio getTorneio(int id) {
		Session session = HConexao.getSessionFactory().openSession();
		session.beginTransaction();
		
		List<Torneio> lista = (List<Torneio>) session.createQuery("FROM Torneio WHERE id=:idTorneio").setParameter("idTorneio", id).list();
		Torneio torneio = (Torneio) ((lista.size() != 0) ? lista.get(0) : null);
		
		session.getTransaction().commit();
		session.close();
		
		return torneio;
	}
}
