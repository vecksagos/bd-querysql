package persistence.dao.jdbc;


import groovyjarjarasm.asm.Type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import persistence.TenistaPersistence;
import model.Tenista;
import util.Conexao;

public class TenistaDao implements TenistaPersistence {

	private Tenista buildTenista(ResultSet rs) {
		Tenista tenista = new Tenista();

		try {
			tenista.setAno_nasc(rs.getInt("ano_nasc"));
			tenista.setApelido(rs.getString("apelido"));
			tenista.setCidade_moradia(rs.getString("cidade_moradia"));
			tenista.setCidade_nascimento(rs.getString("cidade_nascimento"));
			tenista.setId(rs.getInt("id"));
			tenista.setNome(rs.getString("nome"));
			tenista.setEsta_ativo((rs.getInt("esta_ativo") == 1) ? true : false);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return tenista;
	}

	@Override
	public void insert(Tenista tenista) {
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement("INSERT INTO tenista"
					+ "(nome, apelido, ano_nasc, cidade_nascimento, cidade_moradia, esta_ativo) "
					+ "VALUE(?, ?, ?, ?, ?, ?)");

			stmt.setString(1, tenista.getNome());
			stmt.setString(2, tenista.getApelido());
			stmt.setInt(3, tenista.getAno_nasc());
			stmt.setString(4, tenista.getCidade_nascimento());
			stmt.setString(5, tenista.getCidade_moradia());
			stmt.setInt(6, (tenista.isEsta_ativo()) ? 1 : 0);

			stmt.executeUpdate();

			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Tenista> select() {
		List<Tenista> listaTenista = new ArrayList<Tenista>();

		Statement stmt;
		try {
			stmt = Conexao.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM tenista");

			while(rs.next()) {
				Tenista tenista = buildTenista(rs);
				tenista.setPadrinho((rs.getInt("id_padrinho") != 0) ? getTenista(rs.getInt("id_padrinho")) : null);

				listaTenista.add(tenista);
			}

			rs.close();
			stmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listaTenista;
	}

	@Override
	public void update(Tenista tenista) {
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement("UPDATE tenista "
					+ "SET nome=?, apelido=?, ano_nasc=?, cidade_nascimento=?, "
					+ "cidade_moradia=?, esta_ativo=?, id_padrinho=?"
					+ " WHERE id=?");

			stmt.setString(1, tenista.getNome());
			stmt.setString(2, tenista.getApelido());
			stmt.setInt(3, tenista.getAno_nasc());
			stmt.setString(4, tenista.getCidade_nascimento());
			stmt.setString(5, tenista.getCidade_moradia());
			stmt.setInt(6, (tenista.isEsta_ativo()) ? 1 : 0);
			if (tenista.getPadrinho() == null)
				stmt.setNull(7, Type.INT);
			else
				stmt.setInt(7, tenista.getPadrinho().getId());
			stmt.setInt(8, tenista.getId());

			stmt.executeUpdate();

			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Tenista tenista) {
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement("DELETE FROM tenista WHERE id=?");
			stmt.setInt(1, tenista.getId());

			stmt.executeUpdate();

			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}	

	@Override
	public Tenista getTenista(int id) {
		Tenista tenista = null;

		try {
			Statement stmt = Conexao.getConnection().createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM tenista WHERE id=" + id);
			if (rs.next()) {
				tenista = buildTenista(rs);

				if (rs.getInt("id_padrinho") != 0) {
					ResultSet ps = stmt.executeQuery("SELECT * FROM tenista WHERE id=" + rs.getInt("id_padrinho"));
					if (ps.next()) 
						tenista.setPadrinho(buildTenista(ps));

					ps.close();
				}
			}

			rs.close();
			stmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return tenista;
	}

	@Override
	public List<Tenista> getApadrinhado(Tenista tenista) {
		List<Tenista> lista = new ArrayList<Tenista>();

		try {
			Statement stmt = Conexao.getConnection().createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM"
					+ " tenista as te, tenista as pa WHERE pa.id=" + tenista.getId() + ""
					+ " and pa.id=te.id_padrinho");

			while(rs.next()) {
				Tenista te = buildTenista(rs);
				te.setPadrinho(null);
				lista.add(te);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}
} 
