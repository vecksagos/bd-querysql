package persistence.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import persistence.TorneioPersistence;
import model.Torneio;
import util.Conexao;

public class TorneioDao implements TorneioPersistence {

	private Torneio buildTorneio(ResultSet rs) {
		Torneio torneio = new Torneio();

		try {
			torneio.setId(rs.getInt("id"));
			torneio.setLocal(rs.getString("local"));
			torneio.setNome(rs.getString("nome"));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return torneio;
	}

	@Override
	public void insert(Torneio torneio) {
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement("INSERT INTO torneio(nome, local)"
					+ " VALUE(?, ?)");
			stmt.setString(1, torneio.getNome());
			stmt.setString(2, torneio.getLocal());
			
			stmt.executeUpdate();
			
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Torneio> select() {
		List<Torneio> listaTorneio = new ArrayList<Torneio>();
		
		try {
			Statement stmt = Conexao.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM torneio");
			
			while(rs.next()) {
				Torneio torneio = buildTorneio(rs);
				listaTorneio.add(torneio);
			}
			
			rs.close();
			stmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listaTorneio;
	}

	@Override
	public void update(Torneio torneio) {
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement("UPDATE torneio SET nome=?, local=? WHERE id=?");
			stmt.setString(1, torneio.getNome());
			stmt.setString(2, torneio.getLocal());
			stmt.setInt(3, torneio.getId());
		
			stmt.executeUpdate();
			
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Torneio torneio) {
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement("DELETE FROM torneio WHERE id=?");
			stmt.setInt(1, torneio.getId());
			
			stmt.executeUpdate();
			
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}	

	@Override
	public Torneio getTorneio(int id) {
		Torneio torneio = null;

		try {
			Statement stmt = Conexao.getConnection().createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM torneio WHERE id=" + id);
			if (rs.next()) 
				torneio = buildTorneio(rs);
			
			
			rs.close();
			stmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return torneio;
	}
}
