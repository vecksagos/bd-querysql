package persistence.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Participar;
import model.Tenista;
import model.Torneio;
import util.Conexao;
import bo.TenistaBo;
import bo.TorneioBo;

public class ParticiparDao {

	private Participar buildParticipar(ResultSet rs	) {
		Participar participar = new Participar();
		TorneioBo torneioBo = new TorneioBo();
		TenistaBo tenistaBo = new TenistaBo();

		try {
			participar.setAno(rs.getInt("ano"));
			participar.setFoi_campeao((rs.getInt("foi_campeao") == 1) ? true : false);
			participar.setFoi_finalista((rs.getInt("foi_finalista") == 1) ? true : false);
			participar.setPremio(rs.getDouble("premio"));
			participar.setTenista(tenistaBo.getById(rs.getInt("id_tenista")));
			participar.setTorneio(torneioBo.getById(rs.getInt("id_torneio")));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return participar;
	}

	public int insert(Participar participar) {
		int modificado = 0;

		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement("INSERT INTO participar"
					+ "(id_tenista, id_torneio, ano, premio, foi_finalista, foi_campeao) "
					+ "VALUE(?, ?, ?, ?, ?, ?)");

			stmt.setInt(1, participar.getTenista().getId());
			stmt.setInt(2, participar.getTorneio().getId());
			stmt.setInt(3, participar.getAno());
			stmt.setDouble(4, participar.getPremio());
			stmt.setInt(5, ((participar.isFoi_finalista())) ? 1 : 0);
			stmt.setInt(6, ((participar.isFoi_campeao())) ? 1 : 0); 

			modificado = stmt.executeUpdate();

			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return modificado;
	}

	public List<Participar> select() {
		List<Participar> listaParticipar = new ArrayList<Participar>();
		try {
			Statement stmt = Conexao.getConnection().createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM participar");

			while(rs.next()) {
				Participar participar = buildParticipar(rs);
				listaParticipar.add(participar);
			}

			rs.close();
			stmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		}


		return listaParticipar;
	}

	public int update(Participar participar) {
		int modificado = 0;

		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement("UPDATE participar "
					+ "SET premio=?, foi_finalista=?, foi_campeao=? "
					+ "WHERE id_tenista=? and id_torneio=? and ano=?");


			stmt.setDouble(1, participar.getPremio());
			stmt.setInt(2, (participar.isFoi_finalista()) ? 1 : 0);
			stmt.setInt(3, (participar.isFoi_campeao()) ? 1 : 0);
			stmt.setInt(4, participar.getTenista().getId());
			stmt.setInt(5, participar.getTorneio().getId());
			stmt.setInt(6, participar.getAno());

			modificado = stmt.executeUpdate();

			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return modificado;
	}

	public int delete(Participar participar) {
		int modificado = 0;

		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement("DELETE FROM participar "
					+ "WHERE id_tenista=? and id_torneio=? and ano=?");
			stmt.setInt(1, participar.getTenista().getId());
			stmt.setInt(2, participar.getTorneio().getId());
			stmt.setInt(3, participar.getAno());

			modificado = stmt.executeUpdate();

			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return modificado;
	}	

	public Participar getParticipar(Tenista tenista, Torneio torneio, int ano) {
		Participar participar = null;

		try {
			Statement stmt = Conexao.getConnection().createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM participar "
					+ "WHERE id_tenista=" + tenista.getId() + " "
					+ "and id_torneio=" + torneio.getId() + " "
					+ "and ano=" + ano);
			if (rs.next()) 
				participar = buildParticipar(rs);


			rs.close();
			stmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return participar;
	}
	
	public List<Participar> deletaTenista(Tenista tenista) {
		List<Participar> lista = new ArrayList<Participar>();
		
		Statement stmt;
		try {
			stmt = Conexao.getConnection().createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT * FROM participar WHERE id_tenista=" + tenista.getId());
			while (rs.next()) {
				Participar participar = buildParticipar(rs);
				lista.add(participar);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lista;
	}
	
	public List<Participar> deletaTorneio(Torneio torneio) {
		List<Participar> lista = new ArrayList<Participar>();
		
		Statement stmt;
		try {
			stmt = Conexao.getConnection().createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT * FROM participar WHERE id_torneio=" + torneio.getId());
			while (rs.next()) {
				Participar participar = buildParticipar(rs);
				lista.add(participar);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lista;
	}
	
	public List<Participar> deletar2009() {
		List<Participar> lista = new ArrayList<Participar>();
		
		try {
			Statement stmt = Conexao.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM participar WHERE id_torneio=5 and ano=2009");
			
			while(rs.next()) { 
				Participar par = buildParticipar(rs);
				lista.add(par);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lista;
	}
}
