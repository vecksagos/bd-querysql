package persistence;

import java.util.List;

import model.Tenista;

public interface TenistaPersistence {

	void insert(Tenista tenista);
	List<Tenista> select();
	void update(Tenista tenista);
	void delete(Tenista tenista);
	Tenista getTenista(int id);
	List<Tenista> getApadrinhado(Tenista tenista);
}
