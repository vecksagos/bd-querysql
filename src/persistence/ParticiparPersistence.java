package persistence;

import java.util.List;

import model.Participar;
import model.Tenista;
import model.Torneio;

public interface ParticiparPersistence {

	void insert(Participar participar);
	List<Participar> select();
	void update(Participar participar);
	void delete(Participar participar);
	Participar getParticipar(Tenista tenista, Torneio torneio, int ano);
	List<Participar> deletaTorneio(Torneio torneio);
	List<Participar> deletaTenista(Tenista tenista);
	List<Participar> deleta2009();
}
