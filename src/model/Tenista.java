package model;

public class Tenista {
	
	private int id;
	private String nome;
	private String apelido;
	private int ano_nasc;
	private String cidade_nascimento;
	private String cidade_moradia;
	private boolean esta_ativo;
	private Tenista padrinho;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getApelido() {
		return apelido;
	}
	
	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	
	public int getAno_nasc() {
		return ano_nasc;
	}
	
	public void setAno_nasc(int ano_nasc) {
		this.ano_nasc = ano_nasc;
	}
	
	public String getCidade_nascimento() {
		return cidade_nascimento;
	}
	
	public void setCidade_nascimento(String cidade_nascimento) {
		this.cidade_nascimento = cidade_nascimento;
	}
	
	public String getCidade_moradia() {
		return cidade_moradia;
	}
	
	public void setCidade_moradia(String cidade_moradia) {
		this.cidade_moradia = cidade_moradia;
	}
	
	public boolean isEsta_ativo() {
		return esta_ativo;
	}
	
	public void setEsta_ativo(boolean esta_ativo) {
		this.esta_ativo = esta_ativo;
	}
	
	public Tenista getPadrinho() {
		return padrinho;
	}
	
	public void setPadrinho(Tenista padrinho) {
		this.padrinho = padrinho;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("==========\n");
		builder.append("Id: ");builder.append(id);
		builder.append("\nNome: ");builder.append(nome);
		builder.append("\nApelido: ");builder.append(apelido);
		builder.append("\nNascimento: ");builder.append(ano_nasc);
		builder.append("\nCidade de Nascimento: ");builder.append(cidade_nascimento);
		builder.append("\nCidade de Moradia: ");builder.append(cidade_moradia);
		builder.append("\nEstá ativo: ");builder.append((esta_ativo) ? "Sim" : "Não");
		builder.append("\nPadrinho: ");builder.append((padrinho != null) ? padrinho.getNome() : "Nenhum");
		builder.append("\n");
		
		return builder.toString();
	}
}
