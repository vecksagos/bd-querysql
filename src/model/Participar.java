package model;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

import util.HConexao;


public class Participar implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8091103130366326121L;
	private Tenista tenista;
	private Torneio torneio;
	private int ano;
	private double premio;
	private boolean foi_finalista;
	private boolean foi_campeao;
	
	public Tenista getTenista() {
		return tenista;
	}
	
	public void setTenista(Tenista tenista) {
		this.tenista = tenista;
	}
	
	public Torneio getTorneio() {
		return torneio;
	}
	
	public void setTorneio(Torneio torneio) {
		this.torneio = torneio;
	}
	
	public int getAno() {
		return ano;
	}
	
	public void setAno(int ano) {
		this.ano = ano;
	}
	
	public double getPremio() {
		return premio;
	}
	
	public void setPremio(double premio) {
		this.premio = premio;
	}
	
	public boolean isFoi_finalista() {
		return foi_finalista;
	}
	
	public void setFoi_finalista(boolean foi_finalista) {
		this.foi_finalista = foi_finalista;
	}
	
	public boolean isFoi_campeao() {
		return foi_campeao;
	}
	
	public void setFoi_campeao(boolean foi_campeao) {
		this.foi_campeao = foi_campeao;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("==========");
		builder.append("\nTenista: ");builder.append(tenista.getNome());
		builder.append("\nTorneio: ");builder.append(torneio.getNome());
		builder.append("\nAno: ");builder.append(ano);
		builder.append("\nPrêmio: ");builder.append(premio);
		builder.append("\nFoi Finalista: ");builder.append((foi_finalista) ? "Sim" : "Não");
		builder.append("\nFoi Campeão: ");builder.append((foi_campeao) ? "Sim\n" : "Não\n");
		
		return builder.toString();
	}
}
