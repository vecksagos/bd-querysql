package model;


public class Torneio {
	
	private int id;
	private String nome;
	private String local;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getLocal() {
		return local;
	}
	
	public void setLocal(String local) {
		this.local = local;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("==========");
		builder.append("\nId: ");builder.append(id);
		builder.append("\nNome: ");builder.append(nome);
		builder.append("\nLocal: ");builder.append(local);
		builder.append("\n");
		
		return builder.toString();		
	}
}
