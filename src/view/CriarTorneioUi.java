package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import command.Command;

import model.Torneio;
import net.miginfocom.swing.MigLayout;
import controller.TorneioController;

public class CriarTorneioUi extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField nomeField;
	private JTextField localField;
	/**
	 * Create the frame.
	 */
	public CriarTorneioUi() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 680, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[][grow]", "[][][][][][][][][][]"));
		
		JLabel lblNome = new JLabel("Nome");
		contentPane.add(lblNome, "cell 0 0,alignx trailing");
		
		nomeField = new JTextField();
		contentPane.add(nomeField, "cell 1 0,growx");
		nomeField.setColumns(10);
		
		JLabel lblLocal = new JLabel("Local");
		contentPane.add(lblLocal, "cell 0 1,alignx trailing");
		
		localField = new JTextField();
		contentPane.add(localField, "cell 1 1,growx");
		localField.setColumns(10);
		
		JButton btnCriar = new CriarButton("Criar");
		btnCriar.addActionListener(this);
		contentPane.add(btnCriar, "flowx,cell 1 9,alignx right");
		
		JButton btnFechar = new FecharButton("Fechar");
		btnFechar.addActionListener(this);
		contentPane.add(btnFechar, "cell 1 9");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Command cmd = (Command) e.getSource();
		cmd.execute();
	}
	
	public class CriarButton extends JButton implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public CriarButton(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			Torneio torneio = new Torneio();
			torneio.setNome(CriarTorneioUi.this.nomeField.getText());
			torneio.setLocal(CriarTorneioUi.this.localField.getText());
			
			TorneioController controller = new TorneioController();
			controller.inserirTorneio(torneio);
			
			TorneioUi ui = new TorneioUi();
			ui.setVisible(true);
			CriarTorneioUi.this.dispose();
		}
	}
	
	public class FecharButton extends JButton implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public FecharButton(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			TorneioUi ui = new TorneioUi();
			ui.setVisible(true);
			CriarTorneioUi.this.dispose();
		}
	}
}
