package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import command.Command;

import model.Participar;
import model.Tenista;
import model.Torneio;
import net.miginfocom.swing.MigLayout;
import bo.TenistaBo;
import bo.TorneioBo;
import controller.ParticiparController;

public class CriarParticiparUi extends JFrame implements ActionListener, ItemListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JCheckBox campeaoBox;
	private JCheckBox finalistaBox;
	private JSpinner premioSpinner;
	private JSpinner anoSpinner;
	private JComboBox<Integer> torneioBox;
	private JComboBox<Integer> tenistaBox;
	private JLabel nomeTorneioLabel;
	private JLabel nomeTenistaLabel;

	/**
	 * Create the frame.
	 */
	public CriarParticiparUi() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 680, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[][grow][][][]", "[][][][][][][][][]"));

		JLabel lblTenista = new JLabel("Tenista");
		contentPane.add(lblTenista, "cell 0 0,alignx trailing");
		ParticiparController con = new ParticiparController();

		tenistaBox = new TenistaBox();
		tenistaBox.setModel(new DefaultComboBoxModel<Integer>(con.tenistaBox()));
		tenistaBox.addItemListener(this);
		contentPane.add(tenistaBox, "cell 1 0,growx");

		nomeTenistaLabel = new JLabel("Selecione um tenista");
		contentPane.add(nomeTenistaLabel, "cell 2 0");

		JLabel lblTorneio = new JLabel("Torneio");
		contentPane.add(lblTorneio, "cell 0 1,alignx trailing");

		torneioBox = new TorneioBox();
		torneioBox.setModel(new DefaultComboBoxModel<Integer>(con.torneioBox()));
		torneioBox.addItemListener(this);
		contentPane.add(torneioBox, "cell 1 1,growx");

		nomeTorneioLabel = new JLabel("Selecione um torneio");
		contentPane.add(nomeTorneioLabel, "cell 2 1");

		JLabel lblAno = new JLabel("Ano");
		contentPane.add(lblAno, "cell 0 2");

		anoSpinner = new JSpinner();
		anoSpinner.setModel(new SpinnerNumberModel(new Integer(1990), null, null, new Integer(1)));
		contentPane.add(anoSpinner, "cell 1 2");

		JLabel lblPremio = new JLabel("Premio");
		contentPane.add(lblPremio, "cell 0 3");

		premioSpinner = new JSpinner();
		premioSpinner.setModel(new SpinnerNumberModel(new Double(20000), null, null, new Double(1)));
		contentPane.add(premioSpinner, "cell 1 3");

		JLabel lblFoiFinalista = new JLabel("Foi Finalista?");
		contentPane.add(lblFoiFinalista, "cell 0 4");

		finalistaBox = new JCheckBox("");
		contentPane.add(finalistaBox, "cell 1 4");

		JLabel lblFoiCampeo = new JLabel("Foi Campeão?");
		contentPane.add(lblFoiCampeo, "cell 0 5");

		campeaoBox = new JCheckBox("");
		contentPane.add(campeaoBox, "cell 1 5");

		JButton btnCriar = new CriarButton("Criar");
		btnCriar.addActionListener(this);
		contentPane.add(btnCriar, "flowx,cell 3 8,alignx right");

		JButton btnFechar = new FecharButton("Fechar");
		btnFechar.addActionListener(this);
		contentPane.add(btnFechar, "cell 3 8,alignx right");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Command cmd = (Command) e.getSource();
		cmd.execute();
	}

	public class CriarButton extends JButton implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public CriarButton(String name) {
			super(name);
		}

		@Override
		public void execute() {
			Participar participar = new Participar();
			TenistaBo tenistaBo = new TenistaBo();
			TorneioBo torneioBo = new TorneioBo();

			participar.setTenista(tenistaBo.getById((int) CriarParticiparUi.this.tenistaBox.getSelectedItem()));
			participar.setTorneio(torneioBo.getById((int) CriarParticiparUi.this.torneioBox.getSelectedItem()));
			participar.setAno((int) CriarParticiparUi.this.anoSpinner.getValue());
			participar.setPremio((double) CriarParticiparUi.this.premioSpinner.getValue());
			participar.setFoi_finalista(CriarParticiparUi.this.finalistaBox.isSelected());
			participar.setFoi_campeao(CriarParticiparUi.this.campeaoBox.isSelected());

			ParticiparController controller = new ParticiparController();
			controller.inserirParticipar(participar);

			ParticiparUi ui = new ParticiparUi();
			ui.setVisible(true);
			CriarParticiparUi.this.dispose();
		}
	}

	public class FecharButton extends JButton implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public FecharButton(String name) {
			super(name);
		}

		@Override
		public void execute() {
			ParticiparUi ui = new ParticiparUi();
			ui.setVisible(true);
			CriarParticiparUi.this.dispose();
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
			MyCombo combo = (MyCombo) e.getSource();
			combo.execute();
		}
	}

	public interface MyCombo {
		void execute();
	}

	public class TenistaBox extends JComboBox implements MyCombo {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void execute() {
			TenistaBo bo = new TenistaBo();
			Tenista tenista = bo.getById((int) CriarParticiparUi.this.tenistaBox.getSelectedItem());
			CriarParticiparUi.this.nomeTenistaLabel.setText(tenista.getNome());
		}
	}

	public class TorneioBox extends JComboBox implements MyCombo {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void execute() {
			TorneioBo bo = new TorneioBo();
			Torneio torneio = bo.getById((int) CriarParticiparUi.this.torneioBox.getSelectedItem());
			CriarParticiparUi.this.nomeTorneioLabel.setText(torneio.getNome());
		}
	}
}
