package view;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.JCheckBox;

import command.Command;

import controller.RelatorioController;

public class MainUi extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	JCheckBox chckbxExportarParaPdf;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainUi frame = new MainUi();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainUi() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 680, 480);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmTenista = new TenistaItem("Tenista");
		mntmTenista.addActionListener(this);
		mnFile.add(mntmTenista);
		
		JMenuItem mntmTorneio = new TorneioItem("Torneio");
		mntmTorneio.addActionListener(this);
		mnFile.add(mntmTorneio);
		
		JMenuItem mntmParticipar = new ParticiparItem("Participar");
		mntmParticipar.addActionListener(this);
		mnFile.add(mntmParticipar);
		
		JMenuItem mntmFechar = new FecharItem("Fechar");	
		mntmFechar.addActionListener(this);
		mnFile.add(mntmFechar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblProva = new JLabel("Prova");
		GridBagConstraints gbc_lblProva = new GridBagConstraints();
		gbc_lblProva.insets = new Insets(0, 0, 5, 5);
		gbc_lblProva.gridx = 0;
		gbc_lblProva.gridy = 0;
		contentPane.add(lblProva, gbc_lblProva);
		
		JButton btnRelatorio = new Relatorio1Button("Relatorio 1");
		btnRelatorio.addActionListener(this);
		GridBagConstraints gbc_btnRelatorio = new GridBagConstraints();
		gbc_btnRelatorio.insets = new Insets(0, 0, 5, 5);
		gbc_btnRelatorio.gridx = 0;
		gbc_btnRelatorio.gridy = 2;
		contentPane.add(btnRelatorio, gbc_btnRelatorio);
		
		JButton btnRelatorio_1 = new Relatorio2Button("Relatorio 2");
		btnRelatorio_1.addActionListener(this);
		GridBagConstraints gbc_btnRelatorio_1 = new GridBagConstraints();
		gbc_btnRelatorio_1.insets = new Insets(0, 0, 5, 5);
		gbc_btnRelatorio_1.gridx = 1;
		gbc_btnRelatorio_1.gridy = 2;
		contentPane.add(btnRelatorio_1, gbc_btnRelatorio_1);
		
		JButton btnRelatorio_2 = new Relatorio3Button("Relatorio 3");
		btnRelatorio_2.addActionListener(this);
		GridBagConstraints gbc_btnRelatorio_2 = new GridBagConstraints();
		gbc_btnRelatorio_2.insets = new Insets(0, 0, 5, 0);
		gbc_btnRelatorio_2.gridx = 2;
		gbc_btnRelatorio_2.gridy = 2;
		contentPane.add(btnRelatorio_2, gbc_btnRelatorio_2);
		
		chckbxExportarParaPdf = new JCheckBox("Exportar para Pdf?");
		GridBagConstraints gbc_chckbxExportarParaPdf = new GridBagConstraints();
		gbc_chckbxExportarParaPdf.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxExportarParaPdf.gridx = 0;
		gbc_chckbxExportarParaPdf.gridy = 4;
		contentPane.add(chckbxExportarParaPdf, gbc_chckbxExportarParaPdf);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Command cmd = (Command) e.getSource();
		cmd.execute();
	}
	
	public class Relatorio1Button extends JButton implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public Relatorio1Button(String name) {
			super(name);
		}
		@Override
		public void execute() {
			RelatorioController rc = new RelatorioController();
			rc.criarRelatorio1(MainUi.this.chckbxExportarParaPdf.isSelected());
		}
	}
	
	public class Relatorio2Button extends JButton implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public Relatorio2Button(String name) {
			super(name);
		}
		@Override
		public void execute() {
			RelatorioController rc = new RelatorioController();
			rc.criarRelatorio2(MainUi.this.chckbxExportarParaPdf.isSelected());
		}
	}
	
	public class Relatorio3Button extends JButton implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public Relatorio3Button(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			RelatorioController rc = new RelatorioController();
			rc.criarRelatorio3(MainUi.this.chckbxExportarParaPdf.isSelected());
		}
	}
	
	public class TenistaItem extends JMenuItem implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public TenistaItem(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			TenistaUi ui = new TenistaUi();
			ui.setVisible(true);
		}
	}
	
	public class TorneioItem extends JMenuItem implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public TorneioItem(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			TorneioUi ui = new TorneioUi();
			ui.setVisible(true);
		}
	}
	
	public class ParticiparItem extends JMenuItem implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ParticiparItem(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			ParticiparUi ui = new ParticiparUi();
			ui.setVisible(true);
		}
	}
	
	public class FecharItem extends JMenuItem implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public FecharItem(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			MainUi.this.dispose();
		}
	}
}
