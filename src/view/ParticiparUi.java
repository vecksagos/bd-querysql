package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import command.Command;

import net.miginfocom.swing.MigLayout;
import controller.ParticiparController;

public class ParticiparUi extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	/**
	 * Create the frame.
	 */
	public ParticiparUi() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 680, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[][][][][][][][][]"));

		ParticiparController con = new ParticiparController();
		table = new JTable();
		table.setModel(new DefaultTableModel(
				con.tabelar(),
				new String[] {
					"id_tenista", "Tenista", "id_torneio", "Torneio", "Ano", "Premio", "Foi finalista?", "Foi campe\u00E3o?", "Deletar?"
				}
				) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			Class[] columnTypes = new Class[] {
					Integer.class, String.class, Integer.class, String.class, Integer.class, Double.class, String.class, String.class, Boolean.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
					false, false, false, false, false, true, true, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		JScrollPane js = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		contentPane.add(js, "cell 0 2,grow");
		
		JButton btn2009 = new DButton("2009");
		btn2009.addActionListener(this);
		contentPane.add(btn2009, "cell 0 1, alignx right");

		JButton btnAtualizar = new AtualizarButton("Atualizar");
		btnAtualizar.addActionListener(this);
		contentPane.add(btnAtualizar, "cell 0 1, alignx right");
		
		JButton btnCriar = new CriarButton("Criar");
		btnCriar.addActionListener(this);
		contentPane.add(btnCriar, "flowx,cell 0 1,alignx right");

		JButton btnSalvar = new SalvarButton("Salvar");
		btnSalvar.addActionListener(this);
		contentPane.add(btnSalvar, "cell 0 1");

		JButton btnDeletar = new DeletarButton("Deletar");
		btnDeletar.addActionListener(this);
		contentPane.add(btnDeletar, "cell 0 1");

		JButton btnFechar = new FecharButton("Fechar");
		btnFechar.addActionListener(this);
		contentPane.add(btnFechar, "cell 0 1");
	}

	public void atualizar() {
		ParticiparController con = new ParticiparController();
		DefaultTableModel model = new DefaultTableModel(
				con.tabelar(),
				new String[] {
					"id_tenista", "Tenista", "id_torneio", "Torneio", "Ano", "Premio", "Foi finalista", "Foi campe\u00E3o", "Deletar?"
				}
				) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			Class[] columnTypes = new Class[] {
					Integer.class, String.class, Integer.class, String.class, Integer.class, Double.class, String.class, String.class, Boolean.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
					false, false, false, false, true, true, true, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		};

		table.setModel(model);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Command cmd = (Command) e.getSource();
		cmd.execute();
	}

	public class CriarButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;
		public CriarButton(String name) {
			super(name);
		}
		@Override
		public void execute() {
			CriarParticiparUi ui = new CriarParticiparUi();
			ui.setVisible(true);
			ParticiparUi.this.dispose();
		}
	}

	public class SalvarButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;
		public SalvarButton(String name) {
			super(name);
		}
		@Override
		public void execute() {
			ParticiparController con = new ParticiparController();
			con.salvarParticipar(ParticiparUi.this.table);
			ParticiparUi.this.atualizar();
		}
	}

	public class DeletarButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;
		public DeletarButton(String name) {
			super(name);
		}
		@Override
		public void execute() {
			ParticiparController con = new ParticiparController();
			con.deletarParticipar(ParticiparUi.this.table);
			ParticiparUi.this.atualizar();
		}
	}

	public class FecharButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;
		public FecharButton(String name) {
			super(name);
		}
		@Override
		public void execute() {
			ParticiparUi.this.dispose();
		}
	}
	
	public class AtualizarButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;
		public AtualizarButton(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			ParticiparUi.this.atualizar();
		}
	}
	
	public class DButton extends JButton implements Command {
		private static final long serialVersionUID = 1L;
		public DButton(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			ParticiparController con = new ParticiparController();
			con.delete2009();
			ParticiparUi.this.atualizar();
		}
	}
}
