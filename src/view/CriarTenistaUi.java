package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import command.Command;

import model.Tenista;
import controller.TenistaController;

public class CriarTenistaUi extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField nomeField;
	private JTextField apelidoField;
	private JTextField cidadeNascField;
	private JTextField cidadeMoraField;
	private JCheckBox checkBox;
	private JSpinner spinner;

	/**
	 * Create the frame.
	 */
	public CriarTenistaUi() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 680, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(14, 12, 158, 15);
		contentPane.add(lblNome);
		
		JLabel lblApelido = new JLabel("Apelido");
		lblApelido.setBounds(14, 39, 158, 15);
		contentPane.add(lblApelido);
		
		JLabel lblAnoDeNascimento = new JLabel("Ano de Nascimento");
		lblAnoDeNascimento.setBounds(14, 66, 158, 15);
		contentPane.add(lblAnoDeNascimento);
		
		JLabel lblCidadeDeNascimento = new JLabel("Cidade de Nascimento");
		lblCidadeDeNascimento.setBounds(14, 93, 158, 15);
		contentPane.add(lblCidadeDeNascimento);
		
		JLabel lblCidadeDeMoradia = new JLabel("Cidade de Moradia");
		lblCidadeDeMoradia.setBounds(14, 120, 158, 15);
		contentPane.add(lblCidadeDeMoradia);
		
		JLabel lblEstAtivo = new JLabel("Está ativo?");
		lblEstAtivo.setBounds(14, 147, 158, 15);
		contentPane.add(lblEstAtivo);
		
		JButton btnCriar = new CriarButton("Criar");
		btnCriar.addActionListener(this);
		btnCriar.setBounds(14, 300, 158, 25);
		contentPane.add(btnCriar);
		
		JButton btnFechar = new FecharButton("Fechar");
		btnFechar.addActionListener(this);
		btnFechar.setBounds(181, 300, 317, 25);
		contentPane.add(btnFechar);
		
		nomeField = new JTextField();
		nomeField.setBounds(190, 10, 172, 19);
		contentPane.add(nomeField);
		nomeField.setColumns(10);
		
		apelidoField = new JTextField();
		apelidoField.setBounds(190, 37, 172, 19);
		contentPane.add(apelidoField);
		apelidoField.setColumns(10);
		
		cidadeNascField = new JTextField();
		cidadeNascField.setBounds(190, 91, 172, 19);
		contentPane.add(cidadeNascField);
		cidadeNascField.setColumns(10);
		
		cidadeMoraField = new JTextField();
		cidadeMoraField.setBounds(190, 118, 172, 19);
		contentPane.add(cidadeMoraField);
		cidadeMoraField.setColumns(10);
		
		checkBox = new JCheckBox("");
		checkBox.setBounds(186, 147, 129, 23);
		contentPane.add(checkBox);
		
		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(1990), null, null, new Integer(1)));
		spinner.setBounds(190, 64, 70, 20);
		contentPane.add(spinner);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Command cmd = (Command) e.getSource();
		cmd.execute();
	}
	
	public class CriarButton extends JButton implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public CriarButton(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			Tenista tenista = new Tenista();
			tenista.setNome(CriarTenistaUi.this.nomeField.getText());
			tenista.setApelido(CriarTenistaUi.this.apelidoField.getText());
			tenista.setAno_nasc((int) CriarTenistaUi.this.spinner.getValue());
			tenista.setCidade_nascimento(CriarTenistaUi.this.cidadeNascField.getText());
			tenista.setCidade_moradia(CriarTenistaUi.this.cidadeMoraField.getText());
			tenista.setEsta_ativo(CriarTenistaUi.this.checkBox.isSelected());
			
			TenistaController controller = new TenistaController();
			controller.inserirTenista(tenista);
			
			TenistaUi ui = new TenistaUi();
			ui.setVisible(true);
			CriarTenistaUi.this.dispose();
		}
	}
	
	public class FecharButton extends JButton implements Command {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public FecharButton(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			TenistaUi ui = new TenistaUi();
			ui.setVisible(true);
			CriarTenistaUi.this.dispose();
		}
	}
}
