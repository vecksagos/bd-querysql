package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import command.Command;

import net.miginfocom.swing.MigLayout;
import controller.TorneioController;

public class TorneioUi extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;

	/**
	 * Create the frame.
	 */
	public TorneioUi() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 680, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[][][][][][][][][][]"));
		TorneioController controller = new TorneioController();
		table = new JTable();
		table.setModel(new DefaultTableModel(
				controller.tabelar(),
				new String[] {
					"Id", "Nome", "Local", "Deletar?"
				}
			) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				Class[] columnTypes = new Class[] {
					Integer.class, String.class, String.class, Boolean.class
				};
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
			});
		table.getColumnModel().getColumn(0).setPreferredWidth(46);
		table.getColumnModel().getColumn(1).setPreferredWidth(216);
		table.getColumnModel().getColumn(2).setPreferredWidth(192);
		
		JScrollPane js = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		contentPane.add(js, "cell 0 1,grow");

		JButton btnCriar = new CriarButton("Criar");
		btnCriar.addActionListener(this);
		contentPane.add(btnCriar, "flowx,cell 0 8,alignx right,aligny baseline");

		JButton btnSalvar = new SalvarButton("Salvar");
		btnSalvar.addActionListener(this);
		contentPane.add(btnSalvar, "cell 0 8");

		JButton btnDeletar = new DeletarButton("Deletar");
		btnDeletar.addActionListener(this);
		contentPane.add(btnDeletar, "cell 0 8");

		JButton btnFechar = new FecharButton("Fechar");
		btnFechar.addActionListener(this);
		contentPane.add(btnFechar, "cell 0 8");
	}

	public void atualizar() {
		TorneioController controller = new TorneioController();
		DefaultTableModel model = new DefaultTableModel(
				controller.tabelar(),
				new String[] {
					"Id", "Nome", "Local", "Deletar?"
				}
			) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				Class[] columnTypes = new Class[] {
					Integer.class, String.class, String.class, Boolean.class
				};
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
			};

		table.setModel(model);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Command cmd = (Command) e.getSource();
		cmd.execute();
	}

	public class CriarButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;
		public CriarButton(String name) {
			super(name);
		}
		@Override
		public void execute() {
			CriarTorneioUi ui = new CriarTorneioUi();
			ui.setVisible(true);
			TorneioUi.this.dispose();
		}
	}

	public class SalvarButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;
		public SalvarButton(String name) {
			super(name);
		}
		@Override
		public void execute() {
			TorneioController con = new TorneioController();
			con.salvarTabela(TorneioUi.this.table);
			TorneioUi.this.atualizar();
		}
	}

	public class DeletarButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;
		public DeletarButton(String name) {
			super(name);
		}
		@Override
		public void execute() {
			TorneioController con = new TorneioController();
			con.deletarTorneios(TorneioUi.this.table);
			TorneioUi.this.atualizar();
		}
	}

	public class FecharButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;
		public FecharButton(String name) {
			super(name);
		}
		@Override
		public void execute() {
			TorneioUi.this.dispose();
		}
	}
}
