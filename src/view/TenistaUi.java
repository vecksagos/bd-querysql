package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import command.Command;

import net.miginfocom.swing.MigLayout;
import controller.TenistaController;

public class TenistaUi extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;

	/**
	 * Create the frame.
	 */
	public TenistaUi() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 680, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		TenistaController tenistaController = new TenistaController();
		contentPane.setLayout(new MigLayout("", "[518px,grow]", "[][][][][][][][][][][][]"));
		
		JButton btnCriar5 = new Criar5Button("Criar 5");
		btnCriar5.addActionListener(this);
		contentPane.add(btnCriar5, "flowx,cell 0 11, alignx right");

		JButton btnSalvar = new SalvarButton("Salvar");
		btnSalvar.addActionListener(this);
		
		JButton btnCriar = new CriarButton("Criar");
		btnCriar.addActionListener(this);
		contentPane.add(btnCriar, "flowx,cell 0 11,alignx right");
		
		JButton btnDeletar = new DeletarButton("Deletar");
		btnDeletar.addActionListener(this);
		contentPane.add(btnDeletar, "cell 0 11");
		contentPane.add(btnSalvar, "cell 0 11");

		JButton btnFechar = new FecharButton("Fechar");
		btnFechar.addActionListener(this);
		contentPane.add(btnFechar, "cell 0 11");
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
				tenistaController.tabelar(),
				new String[] {
						"Id", "Nome", "Apelido", "Ano de Nascimento", "Cidade de nascimento", "Cidade de Moradia", "Est\u00E1 ativo?", "Id Padrinho", "Nome do Padrinho", "Deletar?"
				}
				) {
			private static final long serialVersionUID = 1L;
			Class[] columnTypes = new Class[] {
					Integer.class, String.class, String.class, Integer.class, String.class, String.class, String.class, Integer.class, String.class, Boolean.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
					false, true, true, true, true, true, true, true, false, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		
		JScrollPane ps = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		table.getColumnModel().getColumn(0).setPreferredWidth(33);
		table.getColumnModel().getColumn(3).setPreferredWidth(137);
		table.getColumnModel().getColumn(4).setPreferredWidth(101);
		table.getColumnModel().getColumn(5).setPreferredWidth(99);
		table.getColumnModel().getColumn(7).setPreferredWidth(31);
		
		contentPane.add(ps, "cell 0 1,grow");
	}

	public void atualizar() {
		TenistaController tenistaController = new TenistaController();
		DefaultTableModel model = new DefaultTableModel(
				tenistaController.tabelar(),
				new String[] {
						"Id", "Nome", "Apelido", "Ano de Nascimento", "Cidade de nascimento", "Cidade de Moradia", "Est\u00E1 ativo?", "Id Padrinho", "Nome do Padrinho", "Deletar?"
				}
				) {
			private static final long serialVersionUID = 1L;
			Class[] columnTypes = new Class[] {
					Integer.class, String.class, String.class, Integer.class, String.class, String.class, String.class, Integer.class, String.class, Boolean.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
					false, true, true, true, true, true, true, true, false, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		};
		
		table.setModel(model);

		table.getColumnModel().getColumn(0).setPreferredWidth(33);
		table.getColumnModel().getColumn(3).setPreferredWidth(137);
		table.getColumnModel().getColumn(4).setPreferredWidth(101);
		table.getColumnModel().getColumn(5).setPreferredWidth(99);
		table.getColumnModel().getColumn(7).setPreferredWidth(31);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Command cmd = (Command) e.getSource();
		cmd.execute();
	}

	public class SalvarButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;

		public SalvarButton(String name) {
			super(name);
		}

		@Override
		public void execute() {
			TenistaController con = new TenistaController();
			con.salvarTabela(TenistaUi.this.table);
			TenistaUi.this.atualizar();
		}
	}
	
	public class CriarButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;

		public CriarButton(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			CriarTenistaUi ui = new CriarTenistaUi();
			ui.setVisible(true);
			TenistaUi.this.dispose();
		}
	}
	
	public class DeletarButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;

		public DeletarButton(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			TenistaController con = new TenistaController();
			con.deletarTenistas(TenistaUi.this.table);
			TenistaUi.this.atualizar();
		}
	}

	public class FecharButton extends JButton implements Command {

		private static final long serialVersionUID = 1L;

		public FecharButton(String name) {
			super(name);
		}

		@Override
		public void execute() {
			TenistaUi.this.dispose();
		}
	}
	
	public class Criar5Button extends JButton implements Command {

		private static final long serialVersionUID = 1L;

		public Criar5Button(String name) {
			super(name);
		}
		
		@Override
		public void execute() {
			TenistaController con = new TenistaController();
			con.criar5();
			TenistaUi.this.atualizar();
		}
	}
}
